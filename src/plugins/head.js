import { createHead } from '@vueuse/head';

export const plugin = {
  install: (app) => {
    app.use(createHead());
  },
};

export default plugin;
