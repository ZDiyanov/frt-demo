import BaseLayout from '@/components/layouts/Basic';
import TextField from '@/components/common/TextField';
import SelectField from '@/components/common/SelectField';
import Toggle from '@/components/common/Toggle';
import BasicBtn from '@/components/common/BasicBtn';

export const plugin = {
  install: (app) => {
    app.component('BaseLayout', BaseLayout);
    app.component('TextField', TextField);
    app.component('SelectField', SelectField);
    app.component('Toggle', Toggle);
    app.component('BasicBtn', BasicBtn);
  },
};

export default plugin;
