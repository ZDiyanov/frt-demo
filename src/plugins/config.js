import configs from '@/configs';

export const plugin = {
  install: (app) => {
    app.provide('$config', configs);
  },
};

export default plugin;
