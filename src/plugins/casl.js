import { Can, abilitiesPlugin } from '@casl/vue';
import { PureAbility } from '@casl/ability';

const config = {
  useGlobalProperties: true,
};

export const ability = new PureAbility();
export const plugin = {
  install: (app) => {
    app.use(abilitiesPlugin, ability, config);
    app.component(Can.name, Can);
    app.provide('$ability', ability);
  },
};

export default plugin;
