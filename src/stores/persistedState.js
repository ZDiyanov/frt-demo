export const auth = { paths: ['id', 'name', 'email', 'role', 'permissions', 'isActive'] };
export const users = { paths: ['items'] };

export default {
  auth,
  users,
};
