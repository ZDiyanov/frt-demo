import { toRaw } from 'vue';
import { defineStore } from 'pinia';
import { ability } from '@/plugins/casl';
import { isObj, isNull, isNonEmptyArr, isBool, isStr } from '@/utils';
import { isAuthorized } from '@/utils/permissions';
import { auth as initialState } from '@/stores/initialState';
import { auth as persist } from '@/stores/persistedState';

/**
 * @description Is valid
 * @param auth
 * @returns {boolean}
 */
const isValid = (auth) => isObj(auth)
  && isNull(auth.id)
  && isNull(auth.firstName)
  && isNull(auth.lastName)
  && isNull(auth.email)
  && isNull(auth.roleId)
  && !isNonEmptyArr(auth.permissions)
  && isBool(auth.isActive)
  && isObj(auth.redirects);

/**
 * @description Init state
 * @param initialState
 * @returns {*}
 */
const initState = (initialState) => {
  if (!isValid(initialState)) {
    throw Error('Invalid initial auth store state');
  }

  const { id, firstName, lastName, email, role, permissions, isActive, redirects } = initialState;
  return () => ({
    id,
    firstName,
    lastName,
    email,
    role,
    permissions,
    isActive,
    redirects,
  });
};

/**
* @description Set ability
* @param permissions
*/
const setAbility = (permissions) => {
  ability.update(permissions);
};

/**
* @description Revoke ability
*/
const revokeAbility = () => {
  ability.update([]);
};

const getters = {
  userId: ({ id }) => id,
  userName: ({ firstName, lastName }) => `${firstName} ${lastName}`,
  userEmail: ({ email }) => email,
  userRole: ({ role }) => role,
  userPermissions: ({ permissions }) => permissions,
  isActiveUser: ({ isActive }) => isActive,
  isAuthorized: ({ permissions }) => (requestedPermissions) => (
    isAuthorized(toRaw(permissions), requestedPermissions)
  ),
  isLogged: ({ id }) => isStr(id),
  redirectMap: ({ redirects }) => redirects,
};

const actions = {
  login(nextUser) {
    const { id, firstName, lastName, email, roleId, permissions, isActive } = nextUser;

    this.id = id;
    this.firstName = firstName;
    this.lastName = lastName;
    this.email = email;
    this.roleId = roleId;
    this.permissions = permissions;
    this.isActive = isActive;

    setAbility(permissions);
    this.$router.push({ name: 'home' });
  },
  logout() {
    const { id, firstName, lastName, email, roleId, permissions, isActive } = initialState;

    this.id = id;
    this.firstName = firstName;
    this.lastName = lastName;
    this.email = email;
    this.roleId = roleId;
    this.permissions = permissions;
    this.isActive = isActive;

    revokeAbility();
    this.$router.push({ name: 'login' });
  }
};

export const authStore = defineStore('auth', {
  state: initState(initialState),
  getters,
  actions,
  persist,
});

export default authStore;
