export const auth = {
  id: null,
  firstName: null,
  lastName: null,
  email: null,
  roleId: null,
  permissions: [],
  isActive: false,
  redirects: {
    index: '/',
    unauthenticated: '/login',
    authenticated: '/home'
  }
};

export const users = {
  items: [],
  activeItem: {},
};

export default {
  auth,
  users,
};
