import { defineStore } from 'pinia';
import { uid } from 'uid';
import { isObj, isNonEmptyArr, isEmptyObj } from '@/utils';
import { users as initialState } from '@/stores/initialState';
import { users as persist } from '@/stores/persistedState';
import { userRoles } from '@/configs/roles';

import { users as mockedUsersList } from '@/mocks/users';

/**
 * @description Is valid
 * @param users
 * @returns {boolean}
 */
const isValid = (users) => isObj(users)
  && !isNonEmptyArr(users.items)
  && isEmptyObj(users.activeItem);

/**
 * @description Init state
 * @param initialState
 * @returns {*}
 */
const initState = (initialState) => {
  if (!isValid(initialState)) {
    throw Error('Invalid initial users store state');
  }

  const { items, activeItem } = initialState;
  return () => ({
    items,
    activeItem,
  });
};

const getters = {
  users: ({ items }) => items,
  activeUser: ({ activeItem }) => activeItem,
};

const actions = {
  setMockedUsers() {
    const nextMockedUserList = [
      ...mockedUsersList.map(({ id, ...rest }) => ({
        ...rest,
        id: uid(),
        role: userRoles.find(({ id }) => id === rest.roleId).label,
      }))
    ];

    this.items = nextMockedUserList;
  },
  setActiveItem(item) {
    this.activeItem = item;
  },
  createItem(nextItem) {
    const nextItems = [
      {
        ...nextItem,
        id: uid(),
        role: userRoles.find(({ id }) => id === nextItem.roleId).label,
        permissions: [],
        isActive: true,
      },
      ...this.items,
    ];

    this.items = nextItems;
  },
  updateItem(nextItem) {
    const nextItems = [
      nextItem,
      ...this.items.filter(({ id }) => id !== nextItem.id),
    ];

    this.items = nextItems;
  },
  deleteItem(nextItem) {
    const nextItems = [
      ...this.items.filter(({ id }) => id !== nextItem.id),
    ];

    this.items = nextItems;
  },
};

export const userStore = defineStore('user', {
  state: initState(initialState),
  getters,
  actions,
  persist,
});

export default userStore;
