export const users = [
  {
    id: null,
    firstName: 'Danniel',
    lastName: 'Blichman',
    email: 'danniel.blichman@testtask.com',
    roleId: 3,
    permissions: [
      { action: 'search', subject: 'list' },
      { action: 'sort', subject: 'list' },
      { action: 'read', subject: 'user' },
      { action: 'delete', subject: 'user' },
    ],
    isActive: true,
  },
  {
    id: null,
    firstName: 'John',
    lastName: 'Appleseed',
    email: 'john.appleseed@testtask.com',
    roleId: 2,
    permissions: [],
    isActive: true,
  },
  {
    id: null,
    firstName: 'Margarrete',
    lastName: 'Jones',
    email: 'margarrete.jones@testtask.com',
    roleId: 1,
    permissions: [
      { action: 'view', subject: 'profile' },
    ],
    isActive: true,
  },
  {
    id: null,
    firstName: 'Bethany',
    lastName: 'Doe',
    email: 'bethany.doe@testtask.com',
    roleId: 1,
    permissions: [],
    isActive: true,
  },
  {
    id: '',
    firstName: 'Samuel',
    lastName: 'Jackson',
    email: 'samuel.jackson@testtask.com',
    roleId: 1,
    permissions: [],
    isActive: true,
  },
  {
    id: '',
    firstName: 'Persival',
    lastName: 'Blinn',
    email: 'persival.blinn@testtask.com',
    roleId: 3,
    permissions: [
      { action: 'read', subject: 'user' },
    ],
    isActive: false,
  },
];

export default { users };
