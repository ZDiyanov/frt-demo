export const base = {
  id: 1,
  slug: 'role_base_user',
  label: 'User',
  availablePermissionsSets: ['app', 'browsing'],
};

export const guest = {
  id: 2,
  slug: 'role_guest_user',
  label: 'Guest',
  availablePermissionsSets: ['app'],
};

export const admin = {
  id: 3,
  slug: 'role_admin_user',
  label: 'Admin',
  availablePermissionsSets: ['app', 'browsing', 'administrative'],
};

export const userRoles = [base, guest, admin];
export default userRoles;
