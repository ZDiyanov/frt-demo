export const app = {
  id: 1,
  slug: 'permission_set_app',
  label: 'UI Features',
  permissions: [
    { action: 'search', subject: 'list', label: 'Search the use list' },
    { action: 'sort', subject: 'list', label: 'Sort the user list' },
  ],
};

export const browsing = {
  id: 2,
  slug: 'permission_set_browsing',
  label: 'Browsing Permissions',
  permissions: [
    { action: 'create', subject: 'user', label: 'Create a user' },
    { action: 'read', subject: 'user', label: 'View user profile' },
    { action: 'update', subject: 'user', label: 'Modify user profile' },
    { action: 'delete', subject: 'user', label: 'Delete user profile' },
  ],
};

export const administrative = {
  id: 3,
  slug: 'permission_set_administrative',
  label: 'Administrative Permissions',
  permissions: [
    { action: 'create', subject: 'admin', label: 'Create an administrator' },
    { action: 'read', subject: 'admin', label: 'View administrator profile' },
    { action: 'update', subject: 'admin', label: 'Modify administrator profile' },
    { action: 'delete', subject: 'admin', label: 'Delete administrator profile' },
  ],
};

export const permissionList = [ app, browsing, administrative ];
export default permissionList;
