import { createRouter, createWebHistory } from 'vue-router';
import multiguard from 'vue-router-multiguard';
import { basic } from '@/router/routes';
import {
  redirectUnauthenticated,
  redirectAuthenticated,
  redirectUnauthorized,
  redirectFromIndex
} from '@/router/traps';

const config = {
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [...basic],
};

export const router = createRouter(config);

router.beforeEach(
  multiguard([
    redirectUnauthenticated,
    redirectAuthenticated,
    redirectUnauthorized,
    redirectFromIndex,
  ])
);

export default router;
