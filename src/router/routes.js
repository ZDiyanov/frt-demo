import LoginView from '@/views/Login';
import HomeView from '@/views/Home';
import UserView from '@/views/User';

export const basic = [
  {
    path: '/login',
    name: 'login',
    meta: {
      slug: 'view_login',
      auth: false,
      isAuthView: true,
    },
    component: LoginView,
  },
  {
    path: '/',
    name: 'index',
    meta: {
      slug: 'view_index',
      auth: true,
      isAuthView: false,
    },
    component: HomeView,
  },
  {
    path: '/home',
    name: 'home',
    meta: {
      slug: 'view_home',
      auth: true,
      isAuthView: false,
    },
    component: HomeView,
  },
  {
    path: '/user/:id',
    name: 'user',
    meta: {
      slug: 'view_user',
      auth: true,
      isAuthView: false,
      permissions: [
        { action: 'read', subject: 'user' },
      ],
    },
    component: UserView,
  },
];

export default { basic };
