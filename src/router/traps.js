import { isArr } from '@/utils';
import { authStore } from '@/stores/auth';

/**
* @description Is matching
* @param path
* @param urlParts
* @returns {boolean}
*/
const isMatching = (path, ...urlParts) => {
  let hasMatch = false;

  urlParts.forEach(urlPart => {
    if (hasMatch) {
      return false;
    }

    hasMatch = path.indexOf(urlPart) > -1;
  });

  return hasMatch;
};

/**
* @description Requires authentication
* @param to
* @returns {boolean}
*/
const requiresAuth = (to) => to.matched.some(record => record.meta.auth);

/**
* @description Is authentication route
* @param path
* @returns {boolean}
*/
const isAuthRoute = (path) => isMatching(path, 'login');

/**
* @description Is logged
* @returns {*}
*/
const isLogged = () => authStore().isLogged;

/**
* @description Is authorized
* @param to
* @returns {*}
*/
const isAuthorized = (to) => authStore().isAuthorized(to.meta.permissions);

/**
* @description Has permissions
* @param to
* @returns {boolean}
*/
const hasPermissions = (to) => isArr(to.meta.permissions);

/**
* @description Get redirects
* @returns {*}
*/
const getRedirects = () => authStore().redirectMap;

/**
* @description Redirect unauthorized
* @param to
* @param from
* @param next
*/
export const redirectUnauthorized = (to, from, next) => {
  if (!hasPermissions(to) || isAuthorized(to)) {
    return next();
  }

  const { index } = getRedirects();
  next(index);
};

/**
* @description Redirect unauthenticated
* @param to
* @param from
* @param next
*/
export const redirectUnauthenticated = (to, from, next) => {
  const { index, unauthenticated, authenticated } = getRedirects();

  if (requiresAuth(to) && !isLogged()) {
    next(unauthenticated);
  } else if (to.path === index) {
    next(authenticated);
  } else {
    next();
  }
};

/**
* @description Redirect authenticated
* @param to
* @param from
* @param next
*/
export const redirectAuthenticated = (to, from, next) => {
  const { index } = getRedirects();

  if (isAuthRoute(to.path) && isLogged()) {
    return next(index);
  }

  next();
};

/**
* @description Redirect from index
* @param to
* @param from
* @param next
*/
export const redirectFromIndex = (to, from, next) => {
  const { index, authenticated } = getRedirects();

  if (to.path === index) {
    return next(authenticated);
  }
  next();
};

export default {
  redirectUnauthorized,
  redirectUnauthenticated,
  redirectAuthenticated,
  redirectFromIndex,
};
