/**
 * @description Is null
 * @param val
 * @returns {boolean}
 */
export const isNull = val => val === null;

/**
* @description Is array
* @param val
* @returns {boolean}
*/
export const isArr = val => Array.isArray(val);

/**
* @description Is object
* @param val
* @returns {boolean}
*/
export const isObj = val => typeof val === 'object' && !isArr(val) && !isNull(val);

/**
* @description Is number
* @param val
* @returns {boolean}
*/
export const isNum = val => typeof val === 'number' && !Number.isNaN(val);

/**
* @description Is function
* @param val
* @returns {boolean}
*/
export const isFunc = val => typeof val === 'function';

/**
* @description Is string
* @param val
* @returns {boolean}
*/
export const isStr = val => typeof val === 'string';

/**
* @description Is undefined
* @param val
* @returns {boolean}
*/
export const isUndef = val => typeof val === 'undefined';

/**
* @description Is boolean
* @param val
* @returns {boolean}
*/
export const isBool = val => typeof val === 'boolean';

/**
* @description To int
* @param val
* @returns {number}
*/
export const toInt = val => parseInt(val, 10);

/**
* @description Is non empty string
* @param val
* @returns {boolean|boolean}
*/
export const isNonEmptyStr = val => isStr(val) && val !== '';

/**
* @description Is non empty array
* @param val
* @returns {boolean|boolean}
*/
export const isNonEmptyArr = val => isArr(val) && val.length > 0;

/**
* @description Has object field
* @param obj
* @param prop
* @returns {boolean}
*/
export const hasObjField = (obj, prop) => Object.prototype.hasOwnProperty.call(obj, prop);

/**
* @description Has object fields
* @param obj
* @param props
* @returns {boolean}
*/
export const hasObjFields = (obj, props) => {
  if (!isArr(props)) {
    throw new Error('props should be array');
  }

  let hasMissingField = false;
  props.forEach(prop => {
    hasMissingField = !hasObjField(obj, prop);
  });
  return !hasMissingField;
};

/**
* @description Has object method
* @param obj
* @param method
* @returns {boolean}
*/
export const hasObjMethod = (obj, method) => hasObjField(obj, method) && isFunc(obj[method]);

/**
* @description Get object keys
* @param obj
* @returns {Array}
*/
export const getObjKeys = obj => Object.keys(obj);

/**
* @description Has object key
* @param obj
* @param key
* @returns {boolean}
*/
export const hasObjKey = (obj, key) => getObjKeys(obj).includes(key);

/**
* @description Is empty object
* @param obj
* @returns {boolean}
*/
export const isEmptyObj = obj => getObjKeys(obj).length === 0;

/**
* @description Each object key
* @param obj
* @param callback
*/
export const eachObjKey = (obj, callback) => {
  getObjKeys(obj).forEach((key, index) => callback(key, obj[key], index));
};

/**
* @description Each object value
* @param obj
* @param callback
*/
export const eachObjValue = (obj, callback) => {
  eachObjKey(obj, (key, value, index) => callback(value, key, index));
};

/**
 * @description Extract nexted prop
 * @param obj
 * @param keysText
 * @returns {*}
 */
export const extractNestedProp = (obj, keysText) => {
  const keys = keysText.split('.');
  const keysLength = keys.length - 1;
  let keysIndex = 0;
  let isValidKey = true;
  let targetObj = { ...obj };
  let targetProp;
  let nextTarget;

  if (keys.length > 0) {
    while (isValidKey) {
      nextTarget = targetObj[keys[keysIndex]];

      if (keysIndex === keysLength) {
        targetProp = !isUndef(nextTarget) && !isNull(nextTarget)
          ? nextTarget
          : undefined;
        break;
      }

      if (!isObj(nextTarget)) {
        isValidKey = false;
        break;
      }

      targetObj = nextTarget;
      keysIndex += 1;
    }
  }

  return targetProp;
};

/**
 * @desc Hide page scroll
 */
export const hidePageScroll = () => {
  document.documentElement.classList.add('hide-scroll');
};

/**
 * @description Show page scroll
 */
export const showPageScroll = () => {
  document.documentElement.classList.remove('hide-scroll');
};
