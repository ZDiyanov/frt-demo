import { isObj } from '@/utils';

/**
 * @desc Is authorized
 * @param permissions
 * @param requestedPermissions
 * @returns {boolean}
 */
export function isAuthorized(permissions, requestedPermissions) {
  let isAuthorized = true;

  requestedPermissions.forEach(({ action, subject }) => {
    if (!isAuthorized) {
      return false;
    }

    const target = permissions
      .filter((p) => p.subject === subject)
      .find((p) => p.action === action);

    if (!isObj(target)) {
      isAuthorized = false;
      return false;
    }
  });

  return isAuthorized;
}

export default isAuthorized;
